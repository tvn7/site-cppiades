async function changeYear(year) {
    // Charger les images et la couleur
    let { minias, color } = await loadData(year);

    // Retirer le loader
    var load = document.getElementById('load');
    if (load) {
        load.remove();
    }

    // Changer le bouton sélectionné et la couleur de l'app
    var buttons = document.querySelectorAll('.year-selector button');
    buttons.forEach(function(button) {
        button.classList.remove('selected');
    });
    document.getElementById('year' + year).classList.add('selected');
    const root = document.documentElement;
    root.style.setProperty('--appMainColor', color);

    // Retirer le footer
    document.getElementById('footer').style.opacity = 0;

    // Retirer les vidéos affichées
    var videoContainer = document.getElementById("videoContainer");
    const grps = document.querySelectorAll('.video-grp');
    grps.forEach(grp => {
        grp.style.opacity = 0;
    });

    // Scroller en haut de la page
    window.scrollTo({ top: 0, behavior: 'smooth' });

    // Attendre que le fondu sortant soit terminé
    await new Promise(resolve => setTimeout(resolve, 500));
    grps.forEach(grp => {
        videoContainer.removeChild(grp);
    });

    // Afficher les vidéos
    fetch('data/' + year + '/videos.json')
        .then(response => response.json())
        .then(data => {
            data.videos.forEach((video, index) => {
                var grp = document.createElement("a");
                grp.classList.add("video-grp");
                grp.href = video.url;
                grp.target = "_blank";

                // Titre
                var title = document.createElement("p");
                title.textContent = video.titre;
                grp.appendChild(title);

                // Miniature
                grp.appendChild(minias[index]);

                videoContainer.appendChild(grp);
            });

            // Faire apparaître les vidéos avec un fondu
            setTimeout(() => {
                const grps = document.querySelectorAll('.video-grp');
                grps.forEach((grp, index) => {
                    setTimeout(() => {
                        grp.style.opacity = 1;
                    }, index * 200);
                });
            }, 50);

            // Faire apparaître le footer
            document.getElementById('footer').style.opacity = 1;
        })
        .catch(error => console.error('Erreur:', error));
}

async function loadData(year) {
    const response = await fetch('data/' + year + '/videos.json');
    const data = await response.json();
    const minias = await Promise.all(data.videos.map(async video => {
        var minia = document.createElement("img");
        minia.src = "data/" + year + "/" + video.minia;
        await new Promise(resolve => {
            minia.onload = resolve;
        });
        return minia;
    }));
    return { minias, color: data.color };
}

changeYear('2024');
