# Site CPPiades x TVn7

Ce site donne accès aux vidéos des CPPiades réaliser par TVn7 à partir de 2023.

Pour ajouter une années il faut :

- ajouter un boutton dans **index.html** avec la bonne année
- créer un dossier avec le nom de l'année dans **public/data/20XX**
- copier un fichier **videos.json** d'une autre année et le remplir avec les nouvelles données et la couleur
- ajouter les **miniatures** en bonne qualité dans le dossier

> Les liens peuvent être des liens youtube ou des liens vers des fichiers vidéos.